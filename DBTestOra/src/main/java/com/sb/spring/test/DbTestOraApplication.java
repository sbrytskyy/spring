package com.sb.spring.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbTestOraApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbTestOraApplication.class, args);
	}
}
