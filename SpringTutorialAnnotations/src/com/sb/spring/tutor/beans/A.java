package com.sb.spring.tutor.beans;

public class A {
	public void init() {
		System.out.println("initialization logic");
	}

	public void cleanup() {
		System.out.println("destruction logic");
	}
}
