package com.sb.spring.tutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sb.spring.tutor.beans.A;

@Configuration
public class ConfigA {
	@Bean(initMethod = "init", destroyMethod = "cleanup" )
	public A a() {
		return new A();
	}
}