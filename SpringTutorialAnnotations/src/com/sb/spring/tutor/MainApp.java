package com.sb.spring.tutor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.sb.spring.tutor.beans.A;
import com.sb.spring.tutor.beans.B;
import com.sb.spring.tutor.beans.HelloWorld;
import com.sb.spring.tutor.beans.TextEditor;

public class MainApp {
	public static void main(String[] args) {
		AbstractApplicationContext ctx = new AnnotationConfigApplicationContext(TextEditorConfig.class);

		// Let us raise a start event.
		ctx.start();

		TextEditor te = ctx.getBean(TextEditor.class);
		te.spellCheck();

		// now both beans A and B will be available...
		A a = ctx.getBean(A.class);
		B b = ctx.getBean(B.class);

		HelloWorld obj = (HelloWorld) ctx.getBean("helloWorld");
		obj.setMessage("Hello!");
		obj.getMessage();

		// Let us raise a stop event.
		ctx.stop();
	}
}