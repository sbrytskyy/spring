package com.sb.spring.tutor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.sb.spring.tutor.beans.SpellChecker;
import com.sb.spring.tutor.beans.TextEditor;

@Configuration
@Import({ ConfigB.class, HelloWorldConfig.class })
public class TextEditorConfig {
	@Bean
	public TextEditor textEditor() {
		return new TextEditor(spellChecker());
	}

	@Bean
	public SpellChecker spellChecker() {
		return new SpellChecker();
	}
}