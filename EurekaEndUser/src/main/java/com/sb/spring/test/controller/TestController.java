package com.sb.spring.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@RestController
@EnableFeignClients
public class TestController {

	@Autowired
	private EurekaClient eurekaClient;

	@Autowired
    private GreetingClient greetingClient;
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE)
	public String errorHandler(Exception ex) {
		return ex.getMessage();
	}

	@Value("${spring.application.name}")
	private String applicationName;
	
	@GetMapping("/info")
	public String info() {
		return applicationName;
	}
	
	@RequestMapping("/greeting1")
	public String greeting1() throws Exception {
		Application application = eurekaClient.getApplication("spring-cloud-eureka-client");
		List<InstanceInfo> instances = application.getInstances();
		// if (instances == null || instances.isEmpty()) {
		// throw new Exception("No service available");
		// }

		InstanceInfo instanceInfo = instances.get(0);
		String hostname = instanceInfo.getHostName();
		int port = instanceInfo.getPort();

		RestTemplate restTemplate = new RestTemplate();

		String url = "http://" + hostname + ":" + port + "/greeting";

		ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);

		return "RestTemplate: " + entity.getBody();
	}
	
	@RequestMapping("/greeting2")
    public String greeting2() {
		return "FeignClient: " + greetingClient.greeting();
    }
	
}
