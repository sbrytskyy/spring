package com.sb.spring.hello;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Greeting {

	@XmlAttribute
	private long id;
	@XmlAttribute
	private String content;

	public Greeting() {
	}
	
	public Greeting(long id, String content) {
		super();
		this.id = id;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

}
