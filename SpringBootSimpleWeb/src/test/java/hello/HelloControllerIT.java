package hello;

import java.net.MalformedURLException;
import java.net.URL;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.sb.spring.hello.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloControllerIT {

	@LocalServerPort
	private int port;
	
	private URL base;
	
	@Autowired
	private TestRestTemplate template;
	
	@Before
	public void setup() throws MalformedURLException {
		base = new URL("http://localhost:" + port + "/");
	}
	
	
	@Test
	public void testIndex() {
		ResponseEntity<String> response = template.getForEntity(base.toString(), String.class);
		Assert.assertThat(response.getBody(), Matchers.equalTo("Hello, World!"));
		
	}
}
