package hello;

import java.util.Map;

import org.assertj.core.api.BDDAssertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.sb.spring.hello.Application;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = { "management.port=0" })
public class HelloWorldConfigurationTests {

	@LocalServerPort
	private int port;

	@Value("${local.management.port}")
	private int mng;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void testIndex() {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = testRestTemplate.getForEntity("http://localhost:" + port + "/hello-world",
				Map.class);
		BDDAssertions.then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void testMgmt() {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = testRestTemplate.getForEntity("http://localhost:" + mng + "/info", Map.class);
		BDDAssertions.then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);

		Assert.assertEquals(entity.getStatusCode(), HttpStatus.OK);
	}
}
