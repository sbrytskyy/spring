package com.sb.spring.netflix.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@Configuration
@EnableEurekaServer
public class Application {
	
	@GetMapping(value="/")
	public String index() {
		return "Hello, World@";
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
