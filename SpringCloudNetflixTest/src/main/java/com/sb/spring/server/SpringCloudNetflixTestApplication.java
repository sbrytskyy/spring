package com.sb.spring.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudNetflixTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudNetflixTestApplication.class, args);
	}
}
