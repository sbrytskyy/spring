package com.sb.spring.boot.test.controllers;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sb.spring.boot.test.models.Bookmark;

public interface BookmarkRepository extends JpaRepository<Bookmark, Long> {

	Collection<Bookmark> findByAccountUsername(String username);
}
