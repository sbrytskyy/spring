package com.sb.spring.boot.test.controllers;

import java.util.Calendar;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sb.spring.boot.test.models.User;

@Component
public class InitTestData {
	
	@Autowired private UserRepository userService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@PostConstruct
    public void init(){
		logger.info("[@PostConstruct] init");
		
		Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.DAY_OF_MONTH, 11);
		cal.set(Calendar.MONTH, 9);
		cal.set(Calendar.YEAR, 1971);
		
		User user = new User("Serhiy Brytskyy", cal.getTime());
		userService.save(user);

		cal.set(Calendar.DAY_OF_MONTH, 15);
		cal.set(Calendar.MONTH, 5);
		cal.set(Calendar.YEAR, 2006);
		
		user = new User("Taras Brytskyy", cal.getTime());
		userService.save(user);
	}
}
