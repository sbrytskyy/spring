package com.sb.spring.boot.test.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3449291706900620557L;

	public UserNotFoundException(String userId) {
		super("could not find user '" + userId + "'.");
	}
}
