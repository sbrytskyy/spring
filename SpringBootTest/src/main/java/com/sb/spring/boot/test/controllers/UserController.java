package com.sb.spring.boot.test.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sb.spring.boot.test.models.User;

@RestController
public class UserController {

	@Autowired private UserRepository userService;
	
	@RequestMapping(value = "/allUsers", method = RequestMethod.GET)
	Iterable<User> getUserCustomers() {
		return userService.findAll();
	}
}
