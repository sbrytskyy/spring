package com.sb.spring.boot.test.controllers;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sb.spring.boot.test.models.User;

@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long> {
	Page<User> findAll(Pageable pageable);
}
