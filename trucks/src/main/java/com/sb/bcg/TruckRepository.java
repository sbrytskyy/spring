package com.sb.bcg;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TruckRepository extends JpaRepository<Truck, Integer> {
	
	public List<Truck> findByLicenseNumber(String licenseNumber);

}
