package com.sb.bcg;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.springframework.web.servlet.view.xml.MappingJackson2XmlView;

@SpringBootApplication
@RestController
public class TrucksApplication {

	@Autowired
	private TruckRepository repo;

	@GetMapping(value = "/trucks")
	public List<Truck> getTruck() {
		return repo.findAll();
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(code=HttpStatus.BAD_REQUEST)
	public ErrorStatus handleException(Exception ex, HttpServletRequest request) {
		return new ErrorStatus(HttpStatus.BAD_REQUEST, ex.getMessage());
	}

	@PostMapping(value = "/trucks")
	public @ResponseBody ResponseEntity<Truck> addTruck(@RequestBody Truck truck) throws Exception {
		List<Truck> l = repo.findByLicenseNumber(truck.getLicenseNumber());
		if (!l.isEmpty()) {
			//return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			throw new Exception("Truck already exists");
		}

		Truck updated = repo.save(truck);

		return new ResponseEntity<Truck>(updated, HttpStatus.CREATED);
	}

	public static void main(String[] args) {
		SpringApplication.run(TrucksApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(TruckRepository repository) {
		return (args) -> {
			// save a couple of customers
			List<Nickname> nicknames = new ArrayList<>();
			nicknames.add(new Nickname("Some cool nick"));
			repository.save(new Truck("123456", nicknames, "5/9/2017"));
		};
	}
	
	@Bean
	public ViewResolver contentNegotiatingViewResolver() {
	    ContentNegotiatingViewResolver resolver =
	            new ContentNegotiatingViewResolver();

	    List<View> views = new ArrayList<>();
	    views.add(new MappingJackson2XmlView());
	    views.add(new MappingJackson2JsonView());

	    resolver.setDefaultViews(views);
	    return resolver;
	}
}
