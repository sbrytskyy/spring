package com.sb.bcg;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Truck {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String licenseNumber;
	
	@OneToMany(targetEntity=Nickname.class, cascade=CascadeType.ALL)
	private List<Nickname> nicknames;
	private String dateCreated;
	
	public Truck() {
		
	}

	public Truck(String licenseNumber, List<Nickname> nicknames, String dateCreated) {
		super();
		this.licenseNumber = licenseNumber;
		this.nicknames = nicknames;
		this.dateCreated = dateCreated;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public List<Nickname> getNicknames() {
		return nicknames;
	}

	public void setNicknames(List<Nickname> nicknames) {
		this.nicknames = nicknames;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

}
