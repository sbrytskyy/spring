package com.sb.spring.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

	@GetMapping("/")
	public String index() {
		return "Book Application";
	}
	
	@GetMapping("/available")
	public @ResponseBody String available() {
		return "Book available";
	}

	@GetMapping("/checkout")
	public String checkout() {
		return "Checkout";
	}
}
