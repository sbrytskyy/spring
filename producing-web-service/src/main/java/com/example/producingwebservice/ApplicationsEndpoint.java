package com.example.producingwebservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.starstandards.star.ProcessCreditApplication;
import org.starstandards.star.ProcessCreditApplicationResponse;

@Slf4j
@Endpoint
public class ApplicationsEndpoint {
	private static final String NAMESPACE_URI = "http://www.starstandards.org/STAR";

	@Autowired
	public ApplicationsEndpoint() {
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "ProcessCreditApplication")
	@ResponsePayload
	public ProcessCreditApplicationResponse submitApplication(@RequestPayload ProcessCreditApplication application) {
		log.debug("Application: \n{}\n", application.toString());

		ProcessCreditApplicationResponse response = new ProcessCreditApplicationResponse();
		response.setResult("success");
		return response;
	}
}