package guru.springframework.spring5webapp.model;

import java.util.HashSet;
import java.util.Set;

public class Test {

    public static void main(String[] args) {
        Publisher publisher = new Publisher("Mega publisher", "Secret Address");
        Book book = new Book("SuperBook", "ISBN:1");
        Author author = new Author("John", "Doe");

        book.setPublisher(publisher);
        book.getAuthors().add(author);
        author.getBooks().add(book);

        System.out.println(book);
    }
}
