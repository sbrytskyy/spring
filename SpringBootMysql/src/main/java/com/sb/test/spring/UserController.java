package com.sb.test.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	private UserRepository uRepo;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public @ResponseBody Long addNewUser(@RequestParam String name, @RequestParam String email) {
		
		User user = new User(name, email);
		User save = uRepo.save(user);
		return save.getId();
	}
	
	@GetMapping(value="/all")
	public @ResponseBody Iterable<User> getAllUsers() {
		return uRepo.findAll();
	}
}
