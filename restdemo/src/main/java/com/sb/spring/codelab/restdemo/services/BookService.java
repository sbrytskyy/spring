package com.sb.spring.codelab.restdemo.services;

import com.sb.spring.codelab.restdemo.domain.Book;
import com.sb.spring.codelab.restdemo.model.BookProtos;
import com.sb.spring.codelab.restdemo.repositories.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public BookProtos.Book save(BookProtos.Book bookDTO) {
        Book book = new Book(bookDTO.getName(), bookDTO.getAuthor());
        Book savedBook = bookRepository.save(book);
        return BookProtos.Book.newBuilder()
                .setId(savedBook.getId())
                .setName(savedBook.getName())
                .setAuthor(savedBook.getAuthor())
                .build();
    }

    public BookProtos.Bookshelf findAllBooks() {
        Iterable<Book> books = bookRepository.findAll();

        BookProtos.Bookshelf.Builder builder = BookProtos.Bookshelf.newBuilder();
        for (Book book : books) {
            builder.addBooks(BookProtos.Book.newBuilder()
                    .setId(book.getId())
                    .setName(book.getName())
                    .setAuthor(book.getAuthor())
                    .build());
        }

        return builder.build();
    }

    public Optional<BookProtos.Book> get(Long bookId) {
        Optional<Book> book = bookRepository.findById(bookId);
        return book.map(value -> BookProtos.Book.newBuilder()
                .setId(value.getId())
                .setName(value.getName())
                .setAuthor(value.getAuthor())
                .build());
    }
}
