package com.sb.spring.codelab.restdemo.repositories;

import com.sb.spring.codelab.restdemo.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
}
