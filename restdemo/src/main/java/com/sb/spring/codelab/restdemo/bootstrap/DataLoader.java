package com.sb.spring.codelab.restdemo.bootstrap;

import com.sb.spring.codelab.restdemo.model.BookProtos;
import com.sb.spring.codelab.restdemo.services.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class DataLoader implements CommandLineRunner {

    private final BookService bookService;

    public DataLoader(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    @Transactional
    public void run(String... args) {
        BookProtos.Book book1 = BookProtos.Book.newBuilder().setName("Ivanhoe")
                .setAuthor("Walter Scott").build();
        bookService.save(book1);

        BookProtos.Book book2 = BookProtos.Book.newBuilder().setName("The Headless Horseman")
                .setAuthor("Thomas Mayne Reid").build();
        bookService.save(book2);

        BookProtos.Bookshelf allBooks = bookService.findAllBooks();
        System.out.println(allBooks);
    }
}
