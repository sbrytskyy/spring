package com.sb.spring.codelab.restdemo.controllers;

import com.sb.spring.codelab.restdemo.domain.Application;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1/applications")
public class ApplicationController {

    public ApplicationController() {
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createNewCustomer(@RequestBody Application application){
        log.debug("Application: \n%s\n", application);
    }
}
