package com.sb.spring.codelab.restdemo.controllers;

import com.sb.spring.codelab.restdemo.exceptions.NotFoundException;
import com.sb.spring.codelab.restdemo.model.BookProtos;
import com.sb.spring.codelab.restdemo.services.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("")
    public BookProtos.Bookshelf listBooks() {
        return bookService.findAllBooks();
    }

    @GetMapping("/{bookId}")
    @ResponseStatus(HttpStatus.OK)
    public BookProtos.Book getBook(@PathVariable Long bookId) {
        Optional<BookProtos.Book> book = bookService.get(bookId);
        if (book.isEmpty()) {
            throw new NotFoundException(String.format("Book with Id %d doesn't exists.", bookId));
        }
        return book.get();
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public BookProtos.Book createBook(@RequestBody BookProtos.Book book) {
        return bookService.save(book);
    }
}
