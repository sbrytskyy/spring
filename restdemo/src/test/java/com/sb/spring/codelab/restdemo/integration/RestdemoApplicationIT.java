package com.sb.spring.codelab.restdemo.integration;

import com.sb.spring.codelab.restdemo.RestdemoApplication;
import com.sb.spring.codelab.restdemo.model.BookProtos;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestdemoApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestdemoApplicationIT {

    @Autowired
    private TestRestTemplate template;

    @Test
    public void getBook() {
        BookProtos.Book book = template.getForObject("/books/2", BookProtos.Book.class);
        System.out.println("book retrieved: " + book.toString());
    }

    @TestConfiguration
    static class Config {

        @Bean
        RestTemplateBuilder restTemplateBuilder() {
            return new RestTemplateBuilder();
        }

        @Bean
        ProtobufHttpMessageConverter protobufHttpMessageConverter() {
            return new ProtobufHttpMessageConverter();
        }
    }
}
