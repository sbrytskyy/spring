package com.sb.spring.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.sb.spring.zuul.filters.PreSampleFilter;

@EnableZuulProxy
@SpringBootApplication
public class BookAppProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookAppProxyApplication.class, args);
	}
	
	@Bean
	public PreSampleFilter preSampleFilter() {
		return new PreSampleFilter();
	}
}
