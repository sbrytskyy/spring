package com.sb.spring.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.cloud.client.discovery.DiscoveryClient;

@RestController
public class ServiceInstanceController {

//	@Autowired
//	private DiscoveryClient discoveryClient;
	
	@Value("${spring.application.name}")
	private String applicationName;
	
	@GetMapping("/info")
	public String info() {
		return applicationName;
	}
	
    @RequestMapping("/greeting")
    public String greeting() {
        return "Hello from EurekaClient!";
    }
//	
//	@RequestMapping("/service-instances/{applicationName}")
//	public List<ServiceInstance> serviceInstanceByName(@PathVariable String applicationName) {
//		return discoveryClient.getInstances(applicationName);
//	}
}
