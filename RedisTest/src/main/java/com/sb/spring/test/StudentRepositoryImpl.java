package com.sb.spring.test;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

	private static final String KEY = "Student";

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
    
	private HashOperations<String, String, Student> hashOps;
	
	@PostConstruct
    private void init() {
        hashOps = redisTemplate.opsForHash();
    }
    
	@Override
	public Map<String, Student> getAll() {
		return hashOps.entries(KEY);
	}

	@Override
	public void addStudent(Student student) {
		hashOps.put(KEY, student.getId(), student);
	}

}
