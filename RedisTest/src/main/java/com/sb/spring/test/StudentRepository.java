package com.sb.spring.test;

import java.util.Map;

public interface StudentRepository {

	Map<String, Student> getAll();
	
	void addStudent(Student student);
}
