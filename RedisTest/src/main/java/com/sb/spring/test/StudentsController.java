package com.sb.spring.test;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sb.spring.test.Student.Gender;

@RestController
public class StudentsController {
	
	@Autowired
	private StudentRepository repo;
	
	@GetMapping("/students")
	public @ResponseBody Iterable<Student> getAll() {
		return repo.getAll().values();
	}
	
	@GetMapping("/addstudent")
	@ResponseStatus(code=HttpStatus.CREATED)
	public @ResponseBody String addStudent(@RequestParam String name) {
		Student student = new Student(UUID.randomUUID().toString(), name, Gender.MALE, 1);
		repo.addStudent(student);
		return "Saved";
	}

	@PostMapping("/addstudent")
	@ResponseStatus(code=HttpStatus.CREATED)
	public @ResponseBody String createStudent(@RequestBody Student student) {
		student.setId(UUID.randomUUID().toString());
//		Student student = new Student(UUID.randomUUID().toString(), name, Gender.MALE, 1);
		repo.addStudent(student);
		return "Saved";
	}
}
