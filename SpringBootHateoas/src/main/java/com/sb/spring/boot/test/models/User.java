package com.sb.spring.boot.test.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {

	@Id
    @GeneratedValue
	private Long id;
	private String name;
	private Date birthday;

	public User() {
	}

	public User(String name, Date birthday) {
		this.name = name;
		this.birthday = birthday;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getBirthday() {
		return birthday;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", birthday=" + birthday + "]";
	}
}
