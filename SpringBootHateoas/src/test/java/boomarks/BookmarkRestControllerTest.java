package boomarks;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.sb.spring.boot.test.Application;
import com.sb.spring.boot.test.controllers.AccountRepository;
import com.sb.spring.boot.test.controllers.BookmarkRepository;
import com.sb.spring.boot.test.models.Account;
import com.sb.spring.boot.test.models.Bookmark;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class BookmarkRestControllerTest {

	private MockMvc mockMvc;

	private Account account;

	private List<Bookmark> bookmarkList = new ArrayList<>();

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private BookmarkRepository bookmarkRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private AccountRepository accountRepository;

	private String userName = "uranium";

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		Assert.assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

		bookmarkRepository.deleteAllInBatch();
		accountRepository.deleteAllInBatch();

		account = accountRepository.save(new Account(userName, "password"));
		bookmarkList.add(
				bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/1/" + userName, "A description")));
		bookmarkList.add(
				bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/2/" + userName, "A description")));
	}

	@Test
	public void userNotFound() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/george/bookmarks/").content(this.json(new Bookmark()))
				.contentType(contentType)).andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	public void readSingleBookmark() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/" + userName + "/bookmarks/" + this.bookmarkList.get(0).getId()))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(contentType))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id",
						Matchers.is(this.bookmarkList.get(0).getId().intValue())))
				.andExpect(MockMvcResultMatchers.jsonPath("$.uri", Matchers.is("http://bookmark.com/1/" + userName)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.description", Matchers.is("A description")));
	}

	@Test
	public void readBookmarks() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/" + userName + "/bookmarks"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(contentType))
				.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].id",
						Matchers.is(this.bookmarkList.get(0).getId().intValue())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].uri", Matchers.is("http://bookmark.com/1/" + userName)))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].description", Matchers.is("A description")))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].id",
						Matchers.is(this.bookmarkList.get(1).getId().intValue())))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].uri", Matchers.is("http://bookmark.com/2/" + userName)))
				.andExpect(MockMvcResultMatchers.jsonPath("$[1].description", Matchers.is("A description")));
	}

	@Test
	public void createBookmark() throws Exception {
		String bookmarkJson = json(new Bookmark(this.account, "http://spring.io",
				"a bookmark to the best resource for Spring news and information"));

		this.mockMvc.perform(MockMvcRequestBuilders.post("/" + userName + "/bookmarks").contentType(contentType)
				.content(bookmarkJson)).andExpect(MockMvcResultMatchers.status().isCreated());
	}

	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}
}
